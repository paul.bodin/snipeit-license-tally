"""import every important module"""
from setuptools import setup, find_packages

setup(
    name="license-tally",
    version="1.0",
    description="Notify lack of license",
    author=["Paul Bodin"],
    authors_emails=[
        "paulbdin@gmail.com",
    ],
    packages=find_packages(),
    url="https://gitlab.com/paul.bodin/snipeit-license-tally",
    install_requires=[
        "requests",
        "keeper_secrets_manager_core",
        "traceback",
        "urllib3",
    ],
)
