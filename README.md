# SnipeIT License Tally

## Description

This project is meant to be used by LNDS to have some script running automatically to improve Snipe It usage. The code is preventing from running out of licenses or from having expired licenses.

It is meant to be runned by a cron task. It will make it to run automatically, so the warning is automated.

## Usage

### Import requirements

Find import requirements in the setup.py file.  

To install it : 
```
python3 setup.py install
```

### Setup

To make the script running properly, you have to change config.cfg.template and ksm-config.json.template as config.cfg and ksm-config.json and fill it with the rights fields, corresponding to your project. ksm-config.json must be generated directly by Keeper when adding your machine in the corresponding Keeper Application.

The config.cfg file must be setup with right fields wanted in general settings. See the documentation in the file.

### Crontask

Find an example below of a crontask that run the code every monday at 10am :  
```
0 10 * * 1 python3 path/to/license_check.py -d path/to/config.cfg
```

> [crontab.guru](https://crontab.guru/) is an useful website to setup a crontask  

To update the crontab, to make the code running at different time or to add some other scripts running, just run crontab -e.
