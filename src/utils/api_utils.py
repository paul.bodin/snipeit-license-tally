"""import every important module"""
from abc import ABC, abstractmethod
import urllib3
import requests
from .keeper_utils import get_from_keeper

DEFAULT_TIMEOUT = 100

urllib3.disable_warnings()


class ApiCaller(ABC):
    """model class"""

    def __init__(self, endpoint):
        self.endpoint = endpoint
        while self.endpoint[-1] == "/" or self.endpoint[-1] == "\\":
            self.endpoint = self.endpoint[:-1]

    @property
    @abstractmethod
    def token(self):
        """token"""
        return NotImplemented

    @property
    def headers(self):
        """header"""
        return {
            "Authorization": "Bearer " + self.token,
            "Content-Type": "application/json",
        }

    @abstractmethod
    def get(self):
        """GET method"""
        return NotImplemented

    def build_url(self, query):
        """complete url"""
        return self.endpoint + query


class SnipeItApiCaller(ApiCaller):
    """api caller class for snipe it"""

    def __init__(self, endpoint, api_key):
        super(SnipeItApiCaller, self).__init__(endpoint)
        self.api_key = api_key

    @property
    def token(self):
        return self.api_key

    def get(self, query, verify=True):
        """GET method"""
        results = []
        url = self.build_url(query)
        res = requests.get(
            url=url, headers=self.headers, verify=verify, timeout=DEFAULT_TIMEOUT
        ).json()
        # results.extend(res["rows"])
        # return results
        return res

    def post(self, query, data, verify=True):
        """POST method"""
        url = self.build_url(query)
        res = requests.post(
            url=url,
            headers=self.headers,
            json=data,
            verify=verify,
            timeout=DEFAULT_TIMEOUT,
        ).json()
        return res

    def patch(self, query, data, verify=True):
        """PATCH method"""
        url = self.build_url(query)
        res = requests.patch(
            url=url,
            headers=self.headers,
            json=data,
            verify=verify,
            timeout=DEFAULT_TIMEOUT,
        ).json()
        return res


def create_snipeit_caller(snipeit_keeper_vault_uid, keeper_file_key_value_storage):
    """method to create an object from the class snipe_it caller"""
    snipeit_endpoint = get_from_keeper(
        "url", snipeit_keeper_vault_uid, keeper_file_key_value_storage
    )
    snipeit_api_key = get_from_keeper(
        "password", snipeit_keeper_vault_uid, keeper_file_key_value_storage
    )
    snipeit_caller = SnipeItApiCaller(
        endpoint=snipeit_endpoint, api_key=snipeit_api_key
    )

    return snipeit_caller
