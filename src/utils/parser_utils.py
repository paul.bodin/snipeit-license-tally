from argparse import ArgumentParser


def parse_args():
    parser = ArgumentParser(
        description="configuration for license tally",
    )
    parser.add_argument(
        "-d",
        "--directory",
        type=str,
        required=True,
        help="path to config file",
        nargs=1,
    )

    return parser.parse_args()
