from sys import stderr
from datetime import date, datetime
from .slack_utils import send_slack_notification


def get_licenses_insuffisiency(
    snipeit_licenses, minimum_seat_left, minimum_seat_left_by_id, webhook_url
):
    minimum_seat_left_by_id = minimum_seat_left_by_id.split(",")
    for x in range(len(minimum_seat_left_by_id)):
        minimum_seat_left_by_id[x] = minimum_seat_left_by_id[x].split("/")

    for snipeit_license in snipeit_licenses["rows"]:
        print(f"{snipeit_license}\n")
        bool_id_verification = False
        for pair in minimum_seat_left_by_id:
            if snipeit_license["id"] == int(pair[0]):
                bool_id_verification = True
                used_seat_left = int(pair[1])
        if bool_id_verification is False:
            used_seat_left = int(minimum_seat_left)

        if snipeit_license["free_seats_count"] <= used_seat_left:
            stderr.write(
                f"There is not enough {snipeit_license['name']} license left ({snipeit_license['free_seats_count']} left out of {snipeit_license['seats']} in total)\n"
            )
            send_slack_notification(
                f"There is not enough {snipeit_license['name']} license left ({snipeit_license['free_seats_count']} left out of {snipeit_license['seats']} in total)\n",
                webhook_url,
            )


def get_expiring_licenses(snipeit_licenses, minimum_days_left, webhook_url):
    for snipeit_license in snipeit_licenses["rows"]:
        today_date = date.today()
        try:
            expiration_date = snipeit_license["expiration_date"]["date"]
            date1 = datetime.strptime(str(today_date), "%Y-%m-%d")
            date2 = datetime.strptime(expiration_date, "%Y-%m-%d")
            date_difference = date2 - date1
            days_difference = date_difference.days
            if days_difference < 0:
                stderr.write(
                    f"{snipeit_license['name']} license has expired (on {snipeit_license['expiration_date']['date']})\n"
                )
                send_slack_notification(
                    f"{snipeit_license['name']} license has expired (on {snipeit_license['expiration_date']['date']})\n",
                    webhook_url,
                )
            elif days_difference <= int(minimum_days_left):
                stderr.write(
                    f"{snipeit_license['name']} license is expiring soon (on {snipeit_license['expiration_date']['date']})\n"
                )
                send_slack_notification(
                    f"{snipeit_license['name']} license is expiring soon (on {snipeit_license['expiration_date']['date']})\n",
                    webhook_url,
                )
        except:
            print("No expiration date")
