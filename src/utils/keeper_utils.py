"""modules to have keeper_utils working correctly"""
from keeper_secrets_manager_core import SecretsManager
from keeper_secrets_manager_core.storage import FileKeyValueStorage


def get_from_keeper(field_wanted, keeper_uid, keeper_file_key_value_storage):
    """get information from keeper as field_wanted = url, login, password..."""
    secrets_manager = SecretsManager(
        config=FileKeyValueStorage(keeper_file_key_value_storage)
    )
    secret_infos = secrets_manager.get_secrets([keeper_uid])[0]
    secret = secret_infos.field(field_wanted, single=True)
    return secret

