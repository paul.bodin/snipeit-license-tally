"""import every important module for slack notification"""
import json
import traceback
import urllib3


def send_slack_notification(message, webhook_url):
    """send Slack notification based on the given message"""
    try:
        slack_message = {"text": message}

        http = urllib3.PoolManager()
        http.request(
            "POST",
            webhook_url,
            body=json.dumps(slack_message),
            headers={"Content-Type": "application/json"},
            retries=False,
        )
    except Exception:
        traceback.print_exc()

    return True
