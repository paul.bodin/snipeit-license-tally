from sys import stderr
from pathlib import Path
from configparser import ConfigParser
from utils.function_utils import get_licenses_insuffisiency, get_expiring_licenses
from utils.api_utils import create_snipeit_caller
from utils.keeper_utils import get_from_keeper
from utils.parser_utils import parse_args


if __name__ == "__main__":
    # BASE_DIR = Path(__file__).resolve().parent.parent

    args = parse_args()

    conf_directory = args.directory

    config = ConfigParser()
    config.read(conf_directory)
    # config.read("config/config.cfg")

    MINIMUM_SEAT_LEFT = config.get("general_settings", "minimum_seat_left_by_default")

    MINIMUM_SEAT_LEFT_BY_ID = config.get("general_settings", "minimum_seat_left_by_id")

    MINIMUM_DAYS_LEFT = config.get("general_settings", "minimum_days_left")

    KEEPER_FILE_KEY_VALUE_STORAGE = config.get(
        "keeper_config_file", "keeper_file_key_value_storage"
    )
    SNIPEIT_KEEPER_VAULT_UID = config.get(
        "keeper_vault_uid", "keeper_vault_uid_of_snipeit"
    )
    SLACK_KEEPER_VAULT_UID = config.get("keeper_vault_uid", "keeper_vault_uid_of_slack")

    WEBHOOK_URL = get_from_keeper(
        "url", SLACK_KEEPER_VAULT_UID, KEEPER_FILE_KEY_VALUE_STORAGE
    )

    snipeit_caller = create_snipeit_caller(
        SNIPEIT_KEEPER_VAULT_UID, KEEPER_FILE_KEY_VALUE_STORAGE
    )
    snipeit_licenses = snipeit_caller.get("/licenses", verify=False)

    get_licenses_insuffisiency(
        snipeit_licenses, MINIMUM_SEAT_LEFT, MINIMUM_SEAT_LEFT_BY_ID, WEBHOOK_URL
    )
    get_expiring_licenses(snipeit_licenses, MINIMUM_DAYS_LEFT, WEBHOOK_URL)
